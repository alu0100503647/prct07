require "lpp_Grupo3/version"
require "lpp_Grupo3/reference"

class LinkedList
	attr_accessor :node
    	
	def initialize()
		@Node = Struct.new(:value, :next)
		@head = nil
		@tail = nil
	end
	
	def insert_by_begin(value)
		if @head.nil?
			@head = @Node.new(value, nil)
			@tail = @head
		else
			@head = @Node.new(value, @head)
		end
	end
		
	def insert_by_end(value)
		if @head.nil?
			@head = @Node.new(value, nil)
			@tail = @head
		else
			@tail[:next] = @Node.new(value, nil)
			@tail = @tail[:next]
		end
	end
		
	def extract_by_begin()
		if @head.nil?
			raise RuntimeError, "List is empty, you can't extract a node"
		else
			if @head == @tail
				@head, @tail = nil
			else
				@head = @head[:next]
			end
			@head[:value]
		end
	end
		
	def extract_by_end()
		if @head.nil?
			raise RuntimeError, "List is empty, you can't extract a node"
		else 
			if @head == @tail
				@head, @tail = nil
			else
				@tail = @head
				while @tail[:next][:next] != nil
					@tail = @tail[:next]
				end
				@tail[:next] = nil
			end
		end
	end
		
	def get_value
		return @head[:value]
	end
    
    def get_next
        return @head[:next]
    end
    
	def size
		size = 0
		iterator = @head
		while (!iterator.nil?)
			iterator = iterator[:next]
			size+=1
		end
		return size
	end
	
	def insert_set(others)
	    for i in (0.. others.size-1)
	        insert_by_end(others[i])
	    end
	end
	
end